// Lesson 3
function fizzBuzz() {
  // your code here
  let i;
  for (i = 1; i <= 100; i++) {
    // number divisible by 3 and 5 will
    // always be divisible by 15, print
    // 'FizzBuzz' in place of the number
    if (i % 15 == 0) console.log("FizzBuzz" + " ");
    // number divisible by 3? print 'Fizz'
    // in place of the number
    else if (i % 3 == 0) console.log("Fizz" + " ");
    // number divisible by 5, print 'Buzz'
    // in place of the number
    else if (i % 5 == 0) console.log("Buzz" + " ");
    // print the number
    else console.log(i + " ");
  }
}
fizzBuzz();
console.log(fizzBuzz);

function filterArray(list) {
  // your code here
  // let formatter = list.filter(el => el.length && typeof el !== 'string' ? true : false )
  // formatter = new Set(formatter.reduce((acc, curr) => [...acc, ...curr], [2, 7,8,9]))
  // return Array.from(formatter).sort((a,b) => b-a )
  // var arr1 = [[2], 23, 'dance', true, [3, 5, 3]],
  // arr2 = [23, 'dance', true],
  // res = arr1.filter(item => !arr2.includes(item));
  // console.log(res);
  fizzBuzz(100);

  //2-task

  const isArr = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (Array.isArray(arr[i])) {
        const answer = [...new Set((arr[0] = arr[0].concat(...arr[i])))];
        console.log(answer.sort((a, b) => a - b));
      }
    }
  };
  // const fruits = ['apple', 'banana']

  // const vegetables = [ ...fruits ] // 0 index
  // console.log(vegetables)
  // Output DON'T TOUCH!
  // console.log(fizzBuzz())
  const letList = [
    [2],
    23,
    "dance",
    [7, 8, 9, 7],
    true,
    [3, 5, 3],
    [777, 855, 9677, 457],
  ];
  console.log(filterArray(letList)); // 2,3,5,7,8,9

  isArr([
    [2],
    23,
    [4, 4, 7, 99, 99],
    [99],
    "dance",
    true,
    [3, 5, 3],
    [1, 6, 7, 89, 101],
  ]);
}
