# Lesson 3

1. Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.

2. Write a program for following condition:
   1. Pass argument (array) to function then return only array element <br>
   2. Sort the array which is filtered in first cycle. <br>
      input: <code>[[2], 23, 'dance', true, [3, 5, 3]]</code><br>
      output: <code>[2, 3, 5]</code> // sorted order<br>
